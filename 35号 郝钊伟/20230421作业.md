```java
 public static void main(String[] args) {
        ArrayList<Student> list = new ArrayList<>();
        Scanner s = new Scanner(System.in);
        out:
        while (true){
            System.out.println("-------欢迎来到学生管理系统--------\n" +
                    "\t\t1 添加学生\n" +
                    "\t\t2 删除学生\n" +
                    "\t\t3 修改学生\n" +
                    "\t\t4 查看所有学生\n" +
                    "\t\t5 退出\n" +
                    "请输入你的选择：");

            String a = s.next();

            switch (a){
                case "1":
//                    System.out.println("1 添加学生");
                    insertStudent(list,s);
                    break;
                case "2":
//                    System.out.println("2 删除学生");
                    deleteStudent(list,s);
                    break;
                case "3":
//                    System.out.println("3 修改学生");
                    updateStudent(list,s);
                    break;
                case "4":
//                    System.out.println("4 查看所有学生");
//                    System.out.println("list = " + list);
                    showStudent(list);
                    break;
                case "5":
                    System.out.println("你已退出系统！");
                    break out;
                default:
                    System.out.println("输入有误，请重新输入");
            }
        }

    }

    /**
     * 添加学生
     * @param list
     * @param s
     */
    public static void insertStudent(ArrayList<Student>list,Scanner s){
        System.out.println("请输入要添加学生的学号：");
        String stuID = s.next();
        if (isExistID(list,stuID)!=-1){
            System.out.println("该学号已存在，请换下一个！");
        }

        Student stu = saveStudent(s, stuID);
        list.add(stu);
        System.out.println("添加成功！");

    }

    /**
     * 将录入的学生信息封装成一个学生对象的方法
     * @param s
     * @param stuID
     * @return 返回一个封装好的学生对象
     */
    private static Student saveStudent(Scanner s, String stuID) {
        System.out.println("请输入姓名：");
        String name = s.next();
        System.out.println("请输入年龄：");
        int age = s.nextInt();
        System.out.println("请输入生日：");
        String birthday = s.next();

        Student stu = new Student(stuID, name, age, birthday);
        return stu;
    }

    /**
     * 判断学号是否存在
     * @param list
     * @param stuID
     * @return 不存在就返回-1，存在就返回找到的位置
     */
    private static int isExistID(ArrayList<Student> list, String stuID) {
        int index =-1;
        if (list.isEmpty()){
            return index;
        }
            for (int i = 0; i < list.size(); i++) {
                Student stu = list.get(i);
                if (stuID.equals(stu.getSid())){
                    index = i;
                }
            }
         return index;
    }

    /**
     * 修改学生对象的方法
     * @param list
     * @param s
     */
    public static void updateStudent(ArrayList<Student>list,Scanner s){
        if (list.isEmpty()){
            System.out.println("空，请返回");
            return;
        }
        System.out.println("请输入要修改学生的学号：");
        String stuID = s.next();
        int index = isExistID(list,stuID);
        if (index==-1){
            System.out.println("你输入的学号不存在，请核实后再修改");
            return;
        }
        Student stu = saveStudent(s, stuID);
        list.set(index,stu);//将集合中index 索引位置替换成stu
        System.out.println("修改成功！");


    }

    /**
     * 查看学生的方法
     * @param list
     */
    public static void showStudent(ArrayList<Student>list){
        if (list.isEmpty()){
            System.out.println("还没有学生，请添加后再来！");
            return;
        }
        System.out.println("学号\t姓名\t年龄\t生日");
        for (int i = 0; i < list.size(); i++) {
            Student stu= list.get(i);
            System.out.println(stu.getSid()+"\t"+stu.getName()+"\t"+stu.getAge()+"\t"+stu.getBirthday());

        }
    }

    /**
     * 删除学生的方法
     * @param list
     * @param s
     */
    public static void deleteStudent(ArrayList<Student>list,Scanner s){
        System.out.println("请输入学生的学号：");
        String stuID = s.next();
        int index = isExistID(list, stuID);
        if (index==-1){
            System.out.println("学号不存在，亲核实后再来！");
            return;
        }
        list.remove(index);
        System.out.println("删除成功！");


    }
```

```java
public class Student {
    private String sid;
    private String name;
    private int age;
    private String birthday;

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "TextStudent{" +
                "sid='" + sid + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", birthday='" + birthday + '\'' +
                '}';
    }


    public Student(String sid, String name, int age, String birthday) {
        this.sid = sid;
        this.name = name;
        this.age = age;
        this.birthday = birthday;
    }
}

```

