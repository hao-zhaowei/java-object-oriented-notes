```java
package hzw;

public class hzw1 {
    public static void main(String[] args) {
//        （1）先声明两个byte类型的变量b1,b2,并分别赋值为10和20，求b1和b2变量的和，并将结果保存在byte类型的变量b3中，
//        最后输出b3变量的值
        byte b1=10;
        byte b2=20;
        byte b3= (byte) (b1+b2);
//        int b3=b1+b2;
        System.out.println(b3);
//        （2）先声明两个short类型的变量s1,s2,并分别赋值为1000和2000，求s1和s2变量的和，并将结果保存在short类型的变量s3中，
//        最后输出s3变量的值
        short s1=1000;
        short s2=2000;
        short s3=(short) (s1+s2);
        System.out.println(s3);

//        （3）先声明1个char类型的变量c1赋值为'a'，再声明一个int类型的变量num赋值为5，求c1和num变量的和，
//        并将结果将结果保存在char类型的变量letter中，最后输出letter变量的值。
        char c1='a';
        int num=5;
//        System.out.println(c1+num);
        char letter=(char) (c1+num);
        System.out.println(letter);

//        （4）先声明两个int类型的变量i1,i2，并分别赋值5和2，求i1和i2的商，并将结果保存在double类型的变量result中，
//        最后输出result变量的值。如何得到结果2.5呢？
        int i1=5;
        int i2=2;
        double result=(double)i1/(double)i2;
        System.out.println(result);

//        1. 定义两个int类型变量a1和a2,分别赋值10,11,判断变量是否为偶数,拼接输出结果
        int a1=10;
        int a2=11;
        System.out.println("10是偶数？" + (a1%2==0));
        System.out.println("11是偶数？" + (a2%2==0));
//        2. 定义两个int类型变量a3和a4,分别赋值12,13,判断变量是否为奇数,拼接输出结果
        int a3=12;
        int a4=13;
        System.out.println("12是奇数？"+(a3%2!=0));
        System.out.println("13是奇数？"+(a4%2!=0));
        
//        为抵抗洪水，战士连续作战89小时，编程计算共多少天零多少小时？
//        1. 定义一个int类型变量hours，赋值为89
        int hours=89;
//        2. 定义一个int类型变量day，用来保存89小时中天数的结果
        int day=(int)hours/24;
//        3. 定义一个int类型变量hour，用来保存89小时中不够一天的剩余小时数的结果
        int hour=hours-day*24;
//        4. 输出结果
        System.out.println("为抵抗洪水，战士连续作战"+hours+"小时："+hours+"是"+day+"天"+hour+"小时");
//        
//        案例：今天是周2，100天以后是周几？
//        1. 定义一个int类型变量week，赋值为2
        int week=2;
        System.out.print("今天是周"+week);
//        2. 修改week的值，在原值基础上加上100
        week=week+100;
//        3. 修改week的值，在原值基础上模以7
        week=week%7;
//        4. 输出结果，在输出结果的时候考虑特殊值，例如周日
        System.out.println(","+"100天以后是周"+week);
        
//        案例：求三个整数x,y,z中的最大值
//        1. 定义三个int类型变量,x,y,z，随意赋值整数值
        int x=1;
        int y=5;
        int z=3;
//        2. 定义一个int类型变量max，先存储x与y中的最大值（使用三元运算符）
        int max= x > y ? x : y;
//        3. 再次对max赋值，让它等于上面max与z中的最大值（使用三元运算符）
        max = max > z ? max : z;
//        4. 输出结果
        System.out.println(max);
        
//        案例：判断今年是否是闰年
//        闰年的判断标准是：
//      1）可以被4整除，但不可被100整除
//      2）可以被400整除
//        1. 定义一个int类型变量year，赋值为今年年份值
        int year = 2023;
//        2. 定一个一个boolean类型变量，用来保存这个年份是否是闰年的结果
        boolean a=year%4==0 & year%100!=0 & year%400==0;
//        3. 输出结果
        if (a==true)
        {
            System.out.println(year+"是闰年");
        }else {
            System.out.println(year+"不是闰年");
        }
//        定义一个double类型变量hua，存储华氏温度80
        double hua=80.0;
//       定义一个double类型变量she，存储摄氏温度，根据公式求值
         double she=(5*(hua-32)/9);
//      输出结果
         System.out.println("华氏度"+hua+"转为摄氏度是"+she);
//    }
}
```

笔记

1、/ 除

2、%取余（模）

3、字符串不需要计算，直接拼接

4、两边只要出现一个字符串就是拼接作用，拼接后得到一个字符串

5、只要前面出现字符串，后面的+都是拼接，除非遇上（），一直向后污染

6、=是赋值，==是做比较

7、短路&&与|| 前一个执行，后一个不执行

8、强制转换：数据类型 变量 = (数据类型)变量、数据

9、三元运算符介绍：

条件表达式 ?  值1 : 值2;
